const router = require("express").Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Get all Users
router.get("/", (req, res) => {
    userController.getUsers().then((result) => res.send(result));
});

//Post new User
router.post("/register", (req, res) => {
    userController.register(req.body).then((result) => {
        res.send(result);
    });
});

//Check Email for duplicate
router.post("/email-exists", (req, res) => {
    userController.checkEmail(req.body.email).then((result) => res.send(result));
});

//POST user login
router.post("/login", (req, res) => {
    userController.login(req.body).then((result) => res.send(result));
});

//GET Details
router.get("/details", auth.verifyToken, (req, res) => {
    // console.log(auth.decodeToken(req.headers.authorization));
    const payload = auth.decodeToken(req.headers.authorization);
    userController.getUserProfile(payload.id).then((result) => res.send(result));
});

//EDIT user
router.put("/:id/edit", (req, res) => {
    userController.editUser(req.params.id, req.body).then((result) => res.send(result));
});

//EDIT user using TOKEN
router.put("/edit", auth.verifyToken, (req, res) => {
    const payload = auth.decodeToken(req.headers.authorization);
    userController.editUserWithToken(payload.id, req.body).then((result) => {
        res.send(result);
    });
});

//Mini activity findOneAndUpdate
router.put("/edit-profile", (req, res) => {
    userController.editDetails(req.body).then((result) => res.send(result));
});

//Mini activity Delete findByIdAndDelete
router.delete("/:id/delete", (req, res) => {
    userController.delete(req.params.id).then((result) => res.send(result));
});

//Mini activity findOneAndDelete
router.delete("/delete", (req, res) => {
    userController.deleteUser(req.body.email).then((result) => res.send(result));
});

// POST Enrollments using TOKEN
router.post("/enroll", auth.verifyToken, (req, res) => {
    const payload = auth.decodeToken(req.headers.authorization);
    const data = { userId: payload.id, courseId: req.body.courseId };
    userController.enroll(data).then((result) => res.send(result));
});
module.exports = router;
