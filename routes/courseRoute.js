const express = require("express");
const router = express.Router();
const courseController = require("./../controllers/courseController");
const auth = require("./../auth");

//create a course
router.post("/create-course", auth.verifyToken, (req, res) => {
    courseController.createCourse(req.body).then((result) => res.send(result));
});

//retrieve all courses
router.get("/", (req, res) => {
    courseController.getAllCourses().then((result) => res.send(result));
});

//retrieving only active courses
router.get("/active-courses", auth.verifyToken, (req, res) => {
    courseController.getActiveCourses().then((result) => res.send(result));
});

//get a specific course using findOne()
router.get("/specific-course", auth.verifyToken, (req, res) => {
    courseController.getSpecificCourse(req.body.courseName).then((result) => res.send(result));
});

//delete course using findOneAndDelete()
router.delete("/delete-course", auth.verifyToken, (req, res) => {
    courseController.deleteCourse(req.body.courseName).then((result) => res.send(result));
});

//delete course using findByIdAndDelete()
router.delete("/:courseId/delete-course", auth.verifyToken, (req, res) => {
    courseController.deleteCourseById(req.params.courseId).then((result) => res.send(result));
});

// edit by ID
router.put("/:courseId/edit", auth.verifyToken, (req, res) => {
    courseController.editCourse(req.params.courseId, req.body).then((result) => res.send(result));
});

// GET active courses
router.get("/active-courses", auth.verifyToken, (req, res) => {
    courseController.getActiveCourses().then((result) => res.send(result));
});

// GET course by ID
router.get("/:id", auth.verifyToken, (req, res) => {
    courseController.getCourseById(req.params.id).then((result) => res.send(result));
});

module.exports = router;
