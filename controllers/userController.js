const User = require("../models/User");
const auth = require("../auth");
const bcrypt = require("bcrypt");
const Course = require("../models/Course");
const saltRounds = 10;

//GET all users
module.exports.getUsers = () => {
    return User.find().then((result) => result);
};

//POST new user
module.exports.register = (body) => {
    //encrypt password before storing in database
    body.password = bcrypt.hashSync(body.password, saltRounds);
    console.log(body);
    return new User(body).save().then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return result;
        }
    });
};

//Check email for duplicates
module.exports.checkEmail = (email) => {
    return User.findOne({ email: email }).then((result, err) => {
        if (err || result != null) {
            console.log("error:", err);
            return false;
        } else {
            return "valid";
        }
    });
};

//Login using jsonwebtoken
module.exports.login = (body) => {
    console.log(body);
    return User.findOne({ email: body.email }).then((result, err) => {
        //check if password is correct
        const passwordIsTrue = bcrypt.compareSync(body.password, result.password);
        if (err || result == null || !passwordIsTrue) {
            console.log(err);
            return false;
        } else {
            console.log(result);
            return { login: "success", access: auth.createAccessToken(result) };
        }
    });
};

//Get User Profile
module.exports.getUserProfile = (id) => {
    return User.findById(id).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            return result;
        }
    });
};

//Edit User
module.exports.editUser = (id, body) => {
    return User.findByIdAndUpdate(id, body, { returnDocument: "after" }).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            result.password = "{encrypted}";
            return result;
        }
    });
};

//Edit User with TOKEN
module.exports.editUserWithToken = (id, body) => {
    if (body.password) {
        body.password = bcrypt.hashSync(body.password, saltRounds);
    }
    return User.findByIdAndUpdate(id, body, { returnDocument: "after" }).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            result.password = "{encrypted}";
            return result;
        }
    });
};

//mini Activity Using findOneAndUpdate
module.exports.editDetails = (body) => {
    if (body.password) {
        body.password = bcrypt.hashSync(body.password, saltRounds);
    }
    return User.findOneAndUpdate({ email: body.email }, body, { returnDocument: "after" }).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            result.password = "{encrypted}";
            return result;
        }
    });
};

//mini activity delete using findByIdAndDelete
module.exports.delete = (id) => {
    return User.findByIdAndDelete(id).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            return "Successfully deleted user with id: " + id;
        }
    });
};

//mini activity findOneAndDelete
module.exports.deleteUser = (email) => {
    return User.findOneAndDelete({ email: email }).then((result, err) => {
        if (err || result == null) {
            console.log(err);
            return false;
        } else {
            return "Successfully deleted user with email: " + email;
        }
    });
};

//Enroll using User ID
module.exports.enroll = async (data) => {
    const { userId, courseId } = data;
    const userEnroll = await User.findById(userId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            result.enrollments.push({ courseId: courseId });
            return result.save().then((result) => true);
        }
    });
    const courseEnroll = await Course.findById(courseId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            result.enrollees.push({ userId: userId });
            return result.save().then((result) => true);
        }
    });
    if (userEnroll && courseEnroll) {
        return true;
    } else {
        return false;
    }
};
