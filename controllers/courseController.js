const Course = require("./../models/Course");

// GET all Courses
module.exports.getAllCourses = () => {
    return Course.find().then((result) => result);
};

// Create new Course
module.exports.createCourse = (body) => {
    return new Course(body).save().then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return result;
        }
    });
};

// GET all Active Courses
module.exports.getActiveCourses = () => {
    return Course.find({ isActive: true }).then((result) => result);
};

// Get by Course Name
module.exports.getSpecificCourse = (name) => {
    return Course.findOne({ courseName: name }).then((result, err) => {
        if (err || result == null) {
            err ? console.log(err) : null;
            return false;
        } else {
            return result;
        }
    });
};

// DELETE Course by Name
module.exports.deleteCourse = (name) => {
    return Course.findOneAndDelete({ courseName: name }).then((result, err) => {
        if (err || result == null) {
            err ? console.log(err) : null;
            return false;
        } else {
            return result;
        }
    });
};

// DELETE Course by ID
module.exports.deleteCourseById = (id) => {
    return Course.findByIdAndDelete(id).then((result, err) => {
        if (err || result == null) {
            err ? console.log(err) : null;
            return false;
        } else {
            return result;
        }
    });
};

// EDIT/PUT Course by ID
module.exports.editCourse = (id, body) => {
    return Course.findByIdAndUpdate(id, body, { returnDocument: "after" }).then((result, err) => {
        if (err || result == null) {
            err ? console.log(err) : null;
            return false;
        } else {
            return result;
        }
    });
};

// GET active courses
module.exports.getActiveCourses = () => {
    return Course.find({ isActive: true }).then((result, err) => {
        if (err) {
            return false;
        } else {
            return result;
        }
    });
};

// GET course by ID
module.exports.getCourseById = (id) => {
    return Course.findById(id).then((result, err) => {
        if (err) {
            return false;
        } else {
            return result;
        }
    });
};
