const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Please input First Name"],
    },
    lastName: {
        type: String,
        required: [true, "Please input Last Name"],
    },
    email: {
        type: String,
        required: [true, "Please input Email"],
    },
    password: {
        type: String,
        required: [true, "Please input Password"],
    },
    mobileNo: { type: String },
    age: {
        type: Number,
        required: false,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    enrollments: [
        {
            courseId: { type: String, required: [true, "Input Course ID"] },
            enrolledOn: { type: Date, default: new Date() },
            status: { type: String, default: "Enrolled" },
        },
    ],
});

module.exports = mongoose.model("User", userSchema);
