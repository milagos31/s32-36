const jwt = require("jsonwebtoken");
const secret = "#ThisIsMyS3cr3t!";
/*
jsonwebtoken methods:
sign:
verify:
decode:
*/

//Create a JWToken
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };
    //jwt.sign(payload, secret,{optional})
    return jwt.sign(data, secret);
};

//Decode the Token
module.exports.decodeToken = (bearerToken) => {
    //jwt.decode(token, {optional})
    const token = bearerToken.replace("Bearer ", "");
    return jwt.decode(token);
};

//Verify Token
module.exports.verifyToken = (req, res, next) => {
    //jwt.verify(token,secret, function())
    if (req.headers.authorization) {
        const token = req.headers.authorization.replace("Bearer ", "");
        console.log("auth token: verified");
        return jwt.verify(token, secret, (err, _) => {
            err ? res.send("auth: failed") : next();
        });
    } else {
        console.log("auth token: failed");
        res.send(false);
    }
};
