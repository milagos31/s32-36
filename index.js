const express = require("express");
const app = express();
const PORT = 3001;
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");
//install cors, bcrypt and jsonwebtoken
const cors = require("cors"); //middleware

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
//Import routes
app.use("/api/users", userRoute);
app.use("/api/courses", courseRoute);

const mongoose = require("mongoose");
const URI = "mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/courseBooking?retryWrites=true&w=majority";
mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.once("open", () => console.log("connected to MongoDB Atlas"));

app.listen(PORT, () => {
    console.log("server is running at port: ", PORT);
});
